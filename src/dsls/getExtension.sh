#!/bin/sh

case $1 in
    maxima)
        echo "maxdsl"
        ;;
    desired-transforms)
        echo "dtdsl"
        ;;
    transforms)
        echo "ctdsl"
        ;;
    motions)
        echo "motdsl"
        ;;
    kinematics)
        echo "kindsl"
        ;;
    *)
esac
