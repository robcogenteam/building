#!/bin/sh

# This script attempts to create automatically the symbolic links to the
#  grammar files. It requires the DSLs subfolders to be in ../dsls/

for LANG in kinematics transforms motions desired-transforms maxima; do
    if [ ! -e $LANG.xtext ]; then
        ln -s `find ../dsls/$LANG/ -name '*.xtext' | head -n 1` $LANG.xtext
    fi
done


