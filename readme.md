# The RobCoGen-building package

[TOC]

## Introduction

The files in this repository are required to build RobCoGen from its
source code (yes, a repository just for the stuff required to build another
project). RobCoGen is a code generator for robotics software; please refer to
its [website][rcg-website].

This readme provides some information about building RobCoGen from its source
code, and it is not meant to document the usage of RobCoGen itself; refer to
its website for that. However, some of the information of this
readme will help to better understand the tool.

I created RobCoGen using the [Xtext][xtext-url]
framework for Eclipse. RobCoGen is based on a few DSL (Domain Specific
Languages) that I designed. The DSL infrastructure created with Xtext is *not*
meant to be built (ie compiled) outside Eclipse. For this reason, compiling the
source code of RobCoGen outside Eclipse is not an easy task. That is why this
repository exists; the scripts in this repository should be able to perform the
entire build automatically.

The building process works only under Linux. It has been tested on Ubuntu 16.04.
For the building to succeed a few steps from your side are required. See the
rest of this readme file.

Good luck ;)

## Building RobCoGen from source
### Prerequisites
To build a distribution of RobCoGen from the source code, you need:

- a Java Runtime Environment (JRE)
- a Java compiler
- the [Apache ANT](http://ant.apache.org/) tool and [IVY](http://ant.apache.org/ivy/)
- the [GNU Make](http://www.gnu.org/software/make/) tool

In Ubuntu linux, all these tools can be downloaded and installed using the
package manager. As far as Java is concerned, one option is to install the
`openjdk-X-jdk` and `openjdk-X-jre` packages, where `X` is just a version
number.

However, I noticed that unfortunately the installation of IVY might not copy
properly the jar file in the system. If you think that is the case, fetch the
file manually from the web (e.g. [here](https://repo1.maven.org/maven2/org/apache/ivy/ivy/2.4.0/ivy-2.4.0.jar))
and copy it in `/usr/share/ant/lib/`.


### Getting the source code
The source code of RobCoGen is contained in several repositories. The easiest
way of getting all the sources is to clone the git-superproject which contains
all the rest as git submodules:

```
git clone git@bitbucket.org:robcogenteam/robcogen.git robcogen

cd robcogen

git submodule update --init
```

In any case, each individual repository lives within the `RobCoGen-new` project
on BitBucket, which you can find
[here](http://bitbucket.org/account/user/robcogenteam/projects/RCG_NEW).

### The components
#### The Domain Specific Languages
The Robotics Code Generator (RobCoGen) basically consists of five Domain
Specific Languages (DSL). With the term "language" I usually mean the language
itself as well as the corresponding software infrastructure, which includes
the parser, code generators, etc; therefore each "language" is in fact a software
package (and a separate git repository). These packages must all be built to
create a distribution of RobCoGen. 

The languages are five:

1. the kinematics DSL: this is the most important one, it defines the format of
   the robot description file and contains most of the implementation of
   RobCoGen.

2. the coordinate transforms DSL: the language for the specification of
   coordinate transforms, and the corresponding code generators.
  
3. the rigid motions DSL: the language for the specification of rigid motions;
   it is related to the coordinate-transforms DSL

4. the desired-transforms DSL: it simply defines the format of the configuration
   file of RobCoGen where the user specifies which are the transforms he/she is
   interested in.
  
5. the maxima DSL: this language is used internally in RobCoGen to interact with
   the symbolic engine Maxima.


To identify these languages (or "projects", or "packages") five names are used:

1. kinematics
2. transforms
3. motions
4. desired-transforms
5. maxima

You must obtain the source code of the five languages in order to build
RobCoGen (see above). Here are the URL of the individual repositories, if you
do not want to use the submodules (see above):

1. kinematics : `git@bitbucket.org:robcogenteam/dsl_kinematics.git`
2. transforms : `git@bitbucket.org:robcogenteam/dsl_transforms.git`
3. motions    : `git@bitbucket.org:robcogenteam/dsl_motions.git`
4. desired-transforms : `git@bitbucket.org:robcogenteam/dsl_desired_transforms.git`
5. maxima     : `git@bitbucket.org:robcogenteam/dsl_maxima.git`

Note that the user of RobCoGen does not need to know anything about
the languages, besides the Kinematics DSL and the desired-transforms DSL. The
first one defines the format for the definition of the robot model; the second
one is used to write a sort of configuration file. For more information refer
to the [RobCoGen website][rcg-website].

#### The user interface
There is one more component needed for the build, the user interface of
RobCoGen. The individual repository with the source code is:

`git@bitbucket.org:robcogenteam/ui.git`

Also this one is included as a git-submodule of the superproject (see above).

### How to build

This section assumes that you have obtained the source code of all the parts
of RobCoGen (i.e. the DSLs and the user interface), and that they are stored
somewhere on your filesystem.

Please move to the root of *this* repo (robcogen-building) and follow these
simple steps to build:

- In the `src` directory, create a symbolic link to the root
  of the user interface repository. Call the link `frontend`.
  
- In the `src/dsls/` directory, create a symbolic link to the root of each of the DSL
  repositories. Name each link according to the convention described in the
  previous section (e.g. `kinematics/`).
  For example:

```
:::bash
      cd src/dsls/
      ln -s <path-to-kinematics_dsl-root> kinematics
      # do the same for the other DSLs ...
      cd ..
```


- In the folder `src/grammars/`, run the script `auto-create-symlinks.sh`.
  It should create a symbolic link to the grammar file (`.xtext`)
  of each of the DSLs, following the naming convention described above. You must
  create the links to the DSLs folders first, as described in the previous point.

- Cross your fingers.

- Type `make` (with the other hand ;) ). When starting from zero, the process
  will typically take a while, like a few minutes.

**Note**: this package is a git repository. Do not add to version control the
symbolic links you created, as their value is user-dependent. If you cloned
directly the source code repositories within this one, rather than creating
symbolic links, then be even more careful: you do not want to add a git repo
within another one...   

The following sections of the `readme` will give more insights about this
repository and about what is going on during the building.


## Testing the build
If everything goes well with the building, you can try to actually run RobCoGen.

Move into the build folder (e.g. `0build/`, or whatever you hacked into the
Makefile), and launch the `robcogen.sh` script. You will have to pass at least
one argument, with a robot model; you can use the example available it the `etc/`
folder:
```
cd building/0build/
./robcogen.sh ../etc/example.kindsl
```

If it works, you should now see the menu of RobCoGen. Refer to the
[website][rcg-website] for help about the usage of the program.

As explained there, to run RobCoGen you would need the symbolic computation
engine Maxima, which must be installed in your system. In addition, you also
need the simple Maxima libraries for RobCoGen, which can be obtained from yet
another git repository (already included as a git-submodule of the superproject):

`git@bitbucket.org:robcogenteam/maxima-libs.git`

The path to these libraries *must* be properly set in the configuration file
`core.cfg`.

In fact, Maxima is required only when generating code for the coordinate
transforms. If you do not need that, or you simply want to verify whether the
building was successful, then just launch `robcogen.sh`. In any case, of course,
you would need a Java Runtime Environment.


## Additional information

### The building process

This section describes the process of building RobCoGen from the source code
of the DSLs. It should clarify a bit the rationale behind this repository,
as for instance why the Makefile is structured as it is. Such an understanding
would be useful when debugging problems.
The content of this section should sound familiar to the users of the
[Xtext framework][xtext-url].

To compile any DSL, three steps are required:

1. Generate the language infrastructure from the grammar file (`.xtext`). Java
   source files are created during this step.

2. Compile all the `.xtend` source files to `.java` source files. Xtend source
   files are contained in the repository of each DSL as the corresponding code
   generator is written mostly with such a language (and Java).
   Xtend is a language that gets compiled into Java code.

3. Compile all the resulting Java source files. The code includes the language
   infrastructure (generated during the first step), and the code generator
   component; the latter is composed of source files which have been either
   written manually, or generated from a Xtend source (which in turn was
   written manually - see step 2).

The resources which can be subject to development (i.e. to changes) are the
grammar file and the code generator component (source code in Xtend and possibly
Java) of each one of the five DSLs.

The Makefile and the Ant file of this repository allow the user to execute all
the three building steps, for all the DSLs, consistently with the interdependencies
among the languages themselves. 

In fact, RobCoGen is made also of another component, its simple user interface.
The interface is simply another Java project (and thus another git repository).
All the source code of the user interface gets also compiled by the build
system. 


### Content of the repository

In short, this repository contains building scripts (e.g. a Makefile), the
resources to build the DSLs (e.g. the program that generates the language
infrastructure given the grammar), and symbolic links to the folders containing
the source code. These links must be created by you (see above). 

Specifically, you should have found the following files and folders in the
package containing this `readme`. Some other files must be created by you for the
compilation process to succeed.

- `0build/`

    The output of the building process is created here. You can safely remove
    all the content of this folder to force a fresh build (when typing `make`).
    Note that there are also some hidden files (whose name starts with a dot)
    which are used by the Makefile.

- `ant/`

    ANT and IVY files, with building instructions.

- `etc/`

    Various things required at building time. More specifically, the
    sub-directories are the following:
    
    - `mwe2` : the workflow files which specify how to generate the language
      infrastructure. Currently, there is a generic workflow file called
      `GenerateDSL__common.mwe2`, which is suitable for any DSL. The other files
      are just symbolic links to this one, with a different name, as the
      building scripts look for file names in the form
      `GenerateDSL_<DSL-name>.mwe2`. There is one exception for the
      desired-transforms DSL, which has special needs and requires a slightly
      different workflow file.
    
    - `lib/` : the Xtext runtime libraries required to interpret the
      workflow files and generate the language infrastructure, plus other
      libraries (JAR files) required to compile the Java code. The content of
      this directory is fetched automatically using IVY.
    
    - `manifest/`, `project_cfg/` : the files in these folders are used just to
      fool the Xtext runtime and pretend that we are building an Eclipse
      project, even though Eclipse is not running at all. The files are copied
      by the building scripts in the appropriate locations in `build/`.
      As mentioned in the introduction, Xtext languages are not meant to be
      built outside Eclipse, and unfortunate tricks like this one seem the only
      way to get the job done; at least to the best of my knowledge.

    For more information please have a look at the [Xtext][xtext-url]
    documentation.

- `src/`
   
    See the section "How to build" for instructions about the symbolic links to
    be created in these folders.

- The other files already contained in the root are:
    - this `readme.md`
    - the license file
    - `Makefile` : a GNU Make file that implements the dependencies of the
      components and triggers the execution of ANT.



## Copyright notice

Copyright © 2015-2020, Marco Frigerio
All rights reserved.

See the license file for additional information.



[rcg-website]: https://robcogenteam.bitbucket.io
[xtext-url]: http://www.eclipse.org/Xtext/
